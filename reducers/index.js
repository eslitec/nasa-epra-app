import { combineReducers } from 'redux'
import disasterManagement from './disasterManagement'
import tasks from './tasks'

export default combineReducers({
  disasterManagement,
  tasks
})
