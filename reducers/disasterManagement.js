const defaultState = [
  {
    id: '',
    type: '',
    name: '',
    group: '',
    indicators: [{
      id: '',
      type: '',
      name: '',
      value: '',
      unit: ''
    }]
  }
]

export default (state = defaultState, action) => {
  return state
}
