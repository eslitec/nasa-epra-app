import * as types from '../actions/action-types'

const defaultState = {
  userId: null,
  location: {}
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case types.UPDATE_LOCATION:
      return state
    case types.UPDATE_NOTIFICATION_TOKEN:
      return state
    default:
      return state
  }
}
