import React from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'

const disasterImage = {
  'PM2.5': 'https://i.imgur.com/4wbR2Yc.png',
  'typhoon': 'https://i.imgur.com/r55CEd8.png',
  'frozen': 'https://i.imgur.com/TSFyyQu.png'
}

const disaster2Image = (title) => {
  return disasterImage[title]
}

const PlanListItem = ({
  disasterType,
  title,
  indicators
}) => {
  const image = disaster2Image(disasterType)
  return (
    <View style={styles.card}>
      <Image
        source={{uri: image}}
        style={{width: 50, height: 50, marginRight: 15, borderRadius: 25}}
      />
      <View style={styles.wrapText}>
        <View>
          <Text style={styles.disasterText}>{disasterType}</Text>
          <Text style={styles.titleText}>{title}</Text>
        </View>
        <View style={styles.cardBottom}>
          <View>
            {Object.keys(indicators).map((indicator, index) => {
              return <Text style={styles.indicatorText} key={index}>{indicator}{indicators[indicator]}</Text>
            })}
          </View>
          <Text style={styles.indicatorText}>Edit</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    height: 106,
    padding: 15,
    borderColor: '#FAFAFA',
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowOffset: {
      width: 5,
      height: 10
    },
    shadowRadius: 10,
    elevation: 3,
    position: 'relative'
  },
  wrapText: {
    flex: 1,
    justifyContent: 'space-between'
  },
  cardBottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  disasterText: {
    marginBottom: 2,
    fontSize: 18,
    fontWeight: '600',
    color: '#0034B6'
  },
  titleText: {
    fontSize: 14,
    color: '#000000'
  },
  indicatorText: {
    fontSize: 14,
    color: '#9B9B9B'
  }
})

export default PlanListItem
