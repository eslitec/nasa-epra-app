import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'

const Header = (props) => (
  <View style={styles.header}>
    <Image source={require('../assets/images/logo.png')} />
  </View>
)

const styles = StyleSheet.create({
  header: {
    marginTop: getStatusBarHeight(),
    paddingTop: 12,
    paddingLeft: 12,
    height: 56,
    alignItems: 'center'
  }
})

export default Header
