import React from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import PlanListItem from './PlanListItem'

const PlanList = ({ plans }) => {
  if (plans.length === 0) {
    return <Text>No Data</Text>
  } else {
    console.log(plans)
    return (
      <FlatList
        data={plans}
        keyExtractor={(item, index) => item.plan}
        renderItem={({ item }) =>
          <View style={styles.item}>
            <PlanListItem
              disasterType={item.hazard.title}
              title={item.plan}
              indicators={item.index}
            />
          </View>
        }
      />
    )
  }
}

const styles = StyleSheet.create({
  item: {
    marginTop: 4,
    marginBottom: 4
  }
})

export default PlanList
