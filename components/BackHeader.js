import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { Icon } from 'expo'

const BackHeader = () => (
  <TouchableOpacity style={styles.header} onPress={() => this.props.navigation.navigate('Group')}>
    <Icon.Ionicons name='md-arrow-back' size={26} />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  header: {
    marginTop: getStatusBarHeight(),
    paddingTop: 16,
    height: 56
  }
})

export default BackHeader
