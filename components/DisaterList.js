import React from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import DisaterListItem from './DisaterListItem'

const DisatersList = ({ disaters }) => {
  if (disaters.length === 0) {
    return <Text>No Data</Text>
  } else {
    // console.log(disaters)
    return (
      <FlatList
        data={disaters}
        keyExtractor={(item, index) => item.plan}
        renderItem={({ item }) =>
          <View style={styles.item}>
            <DisaterListItem
              disasterType={item.hazard.name.en_name}
              title={item.hazard.title}
              indicators={item.index}
            />
          </View>
        }
      />
    )
  }
}

const styles = StyleSheet.create({
  item: {
    marginTop: 4,
    marginBottom: 4
  }
})

export default DisatersList
