import React from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { getStatusBarHeight } from 'react-native-status-bar-height'

const StatusBarView = ({ children }) => (
  <SafeAreaView>
    <StatusBar translucent />
    <View style={styles.container}>
      {children}
    </View>
  </SafeAreaView>
)

const styles = StyleSheet.create({
  container: {
    paddingTop: getStatusBarHeight()
  }
})

export default StatusBarView
