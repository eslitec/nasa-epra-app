import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const DisaterListItem = ({
  disasterType,
  title,
  indicators
}) => (
  <View style={styles.card}>
    <View style={styles.wrapText}>
      <View>
        <Text style={styles.disasterText}>{disasterType}</Text>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <View style={styles.cardBottom}>
        <View>
          {Object.keys(indicators).map((indicator, index) => {
            return <Text style={styles.indicatorText} key={index}>{indicator}{indicators[indicator]}</Text>
          })}
        </View>
        <Text>Edit</Text>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    height: 106,
    padding: 15,
    borderColor: '#FAFAFA',
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowOffset: {
      width: 5,
      height: 10
    },
    shadowRadius: 10,
    elevation: 3,
    position: 'relative'
  },
  wrapText: {
    justifyContent: 'space-between'
  },
  cardBottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  disasterText: {
    marginBottom: 8,
    fontSize: 13,
    fontWeight: '600',
    color: '#0034B6'
  },
  titleText: {
    fontSize: 12,
    color: '#000000'
  },
  indicatorText: {
    fontSize: 13,
    color: '#9B9B9B'
  }
})

export default DisaterListItem
