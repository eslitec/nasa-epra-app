import { Constants, Permissions, Location } from 'expo'
import { Platform } from 'react-native';

export default async () => {
  if (Platform.OS === 'android' && !Constants.isDevice) {
    return {
      status: 'error',
      location: null
    }
  } else {
    const { status } = await Permissions.askAsync(Permissions.LOCATION)
    if (status !== 'granted') {
      return {
        status: 'denied',
        location: null
      }
    }

    const location = await Location.getCurrentPositionAsync({})
    return {
      status: 'granted',
      location
    }
  }
}

