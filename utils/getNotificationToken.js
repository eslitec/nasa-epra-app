import { Permissions, Notifications } from 'expo'

export default async () => {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  )
  let finalStatus = existingStatus

  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
    finalStatus = status
  }

  if (finalStatus !== 'granted') {
    return { status: finalStatus, token: null }
  }

  let token = await Notifications.getExpoPushTokenAsync()

  return { status: finalStatus, token }
}
