import React from 'react'
import { View, Image, Dimensions } from 'react-native'

const ActionShelter = () => (
  <View>
    <Image
      source={require('../assets/images/map.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'contain'}}
    />
  </View>
)

export default ActionShelter
