import React from 'react'
import { Image, Dimensions, TouchableOpacity } from 'react-native'

const DisaterEdit = (props) => (
  <TouchableOpacity onPress={() => props.navigation.navigate('Disaster')}>
    <Image
      source={require('../assets/images/disasterEdit.png')}
      style={{width: Dimensions.get('window').width}}
    />
  </TouchableOpacity>
)

export default DisaterEdit
