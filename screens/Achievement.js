import React from 'react'
import { View, Image, Dimensions } from 'react-native'

const Achievement = () => (
  <View>
    <Image
      source={require('../assets/images/achievement.png')}
      style={{width: Dimensions.get('window').width}}
    />
  </View>
)

export default Achievement
