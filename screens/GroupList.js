import React from 'react'
import { View, Text } from 'react-native'

class GroupList extends React.Component {
  state = {
    groups: null
  }

  async componentDidMount () {
    await this._fetchGroups()
  }

  _fetchGroups = async () => {
    const endpoint = 'https://eprav100.wise-paas.com/api/groups'

    const data  = await fetch(`${endpoint}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({})
    })
    const content = await data.json();
    console.log(content.data)
    this.setState({ groups: content.data })
  }

  render () {
    return (
      <View>
        <Text>GroupList</Text>
      </View>
    )
  }
}

export default GroupList
