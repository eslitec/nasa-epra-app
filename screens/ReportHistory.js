import React from 'react'
import { Image, View, Dimensions, StyleSheet } from 'react-native'

class ReportHistory extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        <Image
          source={require('../assets/images/report.png')}
          style={styles.backgroundImage}
        />
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    width: Dimensions.get('window').width,
    resizeMode: 'cover'
  }
})

export default ReportHistory
