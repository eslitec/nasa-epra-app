import React from 'react'
import { Image, Dimensions, TouchableOpacity } from 'react-native'

const EducationIndex = (props) => (
  <TouchableOpacity onPress={() => props.navigation.navigate('EducationIndexTyphoon')}>
    <Image
      source={require('../assets/images/educationIndex.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'cover'}}
    />
  </TouchableOpacity>
)

export default EducationIndex
