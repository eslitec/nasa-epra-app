import React from 'react'
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import PlanList from '../components/PlanList'

class TaskList extends React.Component {
  state = {
    plan: []
  }

  async componentWillMount () {
    await this._fetchPlan()
  }

  _fetchPlan = async () => {
      const endpoint = 'https://eprav100.wise-paas.com/api/plan'

      const data  = await fetch(`${endpoint}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({})
      })
      const content = await data.json();
      // console.log(content.data)
      this.setState({ plan: content.data })
  }

  render () {
    return (
      <View>
        <View style={styles.container}>
          <PlanList plans={this.state.plan}/>
          <View style={styles.floatBottom}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreatePlan')}>
              <Image source={require('../assets/images/LogoButton.png')} style={styles.logoButton} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 16,
    paddingLeft: 30,
    paddingRight: 30,
  },
  floatBottom: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    bottom: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  floatBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#cccccc'
  },
  logoButton: {
    width: 60,
    height: 60
  }
})

export default TaskList
