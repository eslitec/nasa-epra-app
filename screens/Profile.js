import React from 'react'
import { View, Image, Dimensions } from 'react-native'

const Profile = () => (
  <View>
    <Image
      source={require('../assets/images/profile.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'cover'}}
    />
  </View>
)

export default Profile
