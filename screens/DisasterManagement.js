import React from 'react'
import { Image, Dimensions, TouchableOpacity } from 'react-native'

const DisasterManagement = (props) => (
  <TouchableOpacity onPress={() => props.navigation.navigate('DisasterEdit')}>
    <Image
      source={require('../assets/images/disaster.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'cover'}}
    />
  </TouchableOpacity>
)

export default DisasterManagement
