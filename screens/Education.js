import React from 'react'
import { Image, Dimensions, TouchableOpacity } from 'react-native'

const Education = (props) => (
  <TouchableOpacity onPress={() => props.navigation.navigate('EducationIndex')}>
    <Image
      source={require('../assets/images/education.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'cover'}}
    />
  </TouchableOpacity>
)

export default Education
