import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import t from 'tcomb-form-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { Icon } from 'expo'


const Form = t.form.Form
const User = t.struct({
  "Plan name*": t.String,
  "Group type*": t.String,
  "Disaster type*": t.String,
  "Index*": t.String,
  "To do list*": t.String
});

export default class CreatePlan extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.header} onPress={() => this.props.navigation.goBack()}>
          <Icon.Ionicons name='md-arrow-back' size={26} />
        </TouchableOpacity>
        <Text style={styles.title}>Create a Plan</Text>
        <Form style={styles.form} type={User} />
      </View>
    );
  }
}

import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 50,
    paddingRight: 50,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 18,
    color: "#0034b6",
    marginBottom: 2
  },
  form: {
    textAlign: 'center',
    fontSize: 16,
    marginHorizontal: 10
  },
  header: {
    marginTop: getStatusBarHeight(),
    paddingTop: 16,
    height: 56
  }
})
