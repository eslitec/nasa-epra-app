import React from 'react'
import { View, Image, Dimensions } from 'react-native'

const ActionDistribution = () => (
  <View>
    <Image
      source={require('../assets/images/map.png')}
      style={{width: Dimensions.get('window').width, resizeMode: 'cover'}}
    />
  </View>
)

export default ActionDistribution
