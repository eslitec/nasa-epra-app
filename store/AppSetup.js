import React from 'react'
import { connect } from 'react-redux'
import { Platform, StatusBar, StyleSheet, View } from 'react-native'
import AppNavigator from '../navigation/AppNavigator'
import { updateLocation, updateNotificationToken } from '../actions/settings'
import getLocation from '../utils/getLocation'
import getNotificationToken from '../utils/getNotificationToken'


class AppSetup extends React.Component {
  state = {
    userId: null
  }
  _registerApp = async () => {
    const endpoint = 'https://eprav100.wise-paas.com/api/register'
    const { status: locationStatus, location } = await getLocation()
    const { status: Notificationstatus, token } = await getNotificationToken()
    if (locationStatus === 'granted' && Notificationstatus === 'granted') {
      const data  = await fetch(`${endpoint}/${token}/${location.coords.latitude}/${location.coords.longitude}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({})
      })
      const content = await data.json();
      this.setState({ userId: content.user_uuid })
    }else{
      alert("Open Your Notification")
    }
  }

  async componentWillMount () {
    await this._registerApp()
  }

  render () {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <AppNavigator />
      </View>
   )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
})

const mapDispatchToProps = dispatch => ({
  updateLocation: (location) => dispatch(updateLocation(location)),
  updateNotificationToken: (token) => dispatch(updateNotificationToken(token))
})

export default connect(undefined, mapDispatchToProps)(AppSetup)
