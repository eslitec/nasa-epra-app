import React from 'react'
import { Provider } from 'react-redux'
import AppSetup from './AppSetup'
import configureStore from './configureStore'

const store = configureStore()

const AppProvider = () => (
  <Provider store={store}>
    <AppSetup />
  </Provider>
)

export default AppProvider
