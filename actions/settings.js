import * as types from './action-types'
// import * as settingsApi from '../api/settings.js'

export const updateLocation = () => ({
  type: types.UPDATE_LOCATION
})

export const updateNotification = () => ({
  type: types.UPDATE_NOTIFICATION_TOKEN
})
