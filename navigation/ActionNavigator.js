import React from 'react'
import { createMaterialTopTabNavigator } from 'react-navigation'
import { Icon } from 'expo'
import ActionShelter from '../screens/ActionShelter'
import ActionDistribution from '../screens/ActionDistribution'

const ActionGuide = createMaterialTopTabNavigator({
  Distribution: ActionDistribution,
  Shelter: ActionShelter
}, {
  initialRouteName: 'Distribution',
  tabBarOptions: {
    activeTintColor: '#0034B6',
    activeBackgroundColor: '#FAFAFA',
    inactiveTintColor: '#979797',
    inactiveBackgroundColor: '#FAFAFA',
    style: {
      backgroundColor: '#FAFAFA'
    }
  }
})

ActionGuide.navigationOptions = {
  tabBarLabel: 'Action',
  tabBarIcon: ({ focused }) => (
    <Icon.Entypo
      name='lifebuoy'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default ActionGuide
