import React from 'react'
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation'
import { Icon } from 'expo'

import Education from '../screens/Education'
import EducationIndex from '../screens/EducationIndex'
import EducationIndexTyphoon from '../screens/EducationIndexTyphoon'

const EducationStack = createStackNavigator({
  Education,
  EducationIndex: EducationIndex,
  EducationIndexTyphoon: EducationIndexTyphoon
}, {
  initialRouteName: 'Education',
  headerMode: 'none'
})

const EducationTab = createMaterialTopTabNavigator({
  Preditable: EducationStack,
  Unpreditable: EducationStack
}, {
  initialRouteName: 'Preditable',
  tabBarOptions: {
    activeTintColor: '#0034B6',
    activeBackgroundColor: '#FAFAFA',
    inactiveTintColor: '#979797',
    inactiveBackgroundColor: '#FAFAFA',
    style: {
      backgroundColor: '#FAFAFA'
    }
  }
})

EducationTab.navigationOptions = {
  tabBarLabel: 'Education',
  tabBarIcon: ({ focused }) => (
    <Icon.Entypo
      name='info-with-circle'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default EducationTab
