import React from 'react'
import { View } from 'react-native'
import { createStackNavigator } from 'react-navigation'
import MainNavigator from './MainNavigator'
import Header from '../components/Header'

const MainLayout = () => (
  <View style={{flex: 1}}>
    <Header />
    <MainNavigator />
  </View>
)

export default createStackNavigator({
  Main: MainLayout
}, {
  headerMode: 'none'
})
