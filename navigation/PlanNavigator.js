import React from 'react'
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation'
import { Icon } from 'expo'
import TaskList from '../screens/TaskList'
import DisasterManagement from '../screens/DisasterManagement'
import CreatePlan from '../screens/CreatePlan'
import DisaterEdit from '../screens/DisaterEdit'

const PlanTab = createMaterialTopTabNavigator({
  Disaster: DisasterManagement,
  TaskList
}, {
  initialRouteName: 'TaskList',
  tabBarOptions: {
    activeTintColor: '#0034B6',
    activeBackgroundColor: '#FAFAFA',
    inactiveTintColor: '#979797',
    inactiveBackgroundColor: '#FAFAFA',
    style: {
      backgroundColor: '#FAFAFA'
    }
  }
})

const PlanStack = createStackNavigator({
  Plan: PlanTab,
  CreatePlan: CreatePlan,
  DisasterEdit: DisaterEdit
}, {
  initialRouteName: 'Plan',
  headerMode: 'none'
})

PlanStack.navigationOptions = {
  tabBarLabel: 'Plan',
  tabBarIcon: ({ focused }) => (
    <Icon.Foundation
      name='list-thumbnails'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default PlanStack
