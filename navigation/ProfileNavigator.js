import React from 'react'
import { createMaterialTopTabNavigator } from 'react-navigation'
import { Icon } from 'expo'
import Achievement from '../screens/Achievement'
import Profile from '../screens/Profile'

const ProfileNavigator = createMaterialTopTabNavigator({
  Profile,
  Achievement
}, {
  initialRouteName: 'Profile',
  tabBarOptions: {
    activeTintColor: '#0034B6',
    activeBackgroundColor: '#FAFAFA',
    inactiveTintColor: '#979797',
    inactiveBackgroundColor: '#FAFAFA',
    style: {
      backgroundColor: '#FAFAFA'
    }
  }
})

ProfileNavigator.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <Icon.Ionicons
      name='md-contact'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default ProfileNavigator
