import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { Icon } from 'expo'
import Reporter from '../screens/Reporter'
import ReportHistory from '../screens/ReportHistory'

const ReportStack = createStackNavigator({
  Reporter,
  ReportHistory
}, {
  initialRouteName: 'ReportHistory',
  headerMode: 'none'
})

ReportStack.navigationOptions = {
  tabBarLabel: 'Report',
  tabBarIcon: ({ focused }) => (
    <Icon.Ionicons
      name='md-repeat'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default ReportStack
