import { createBottomTabNavigator } from 'react-navigation'
import EducationNavigator from './EducationNavigator'
import PlanNavigator from './PlanNavigator'
import ReportNavigator from './ReportNavigator'
import ActionNavigator from './ActionNavigator'
// import GroupNavigator from './GroupNavigator'
import ProfileNavigator from './ProfileNavigator'

const MainTabNavigator = createBottomTabNavigator({
  Profile: ProfileNavigator,
  Education: EducationNavigator,
  Plan: PlanNavigator,
  Report: ReportNavigator,
  Action: ActionNavigator
}, {
  initialRouteName: 'Plan',
  tabBarOptions: {
    style: {
      height: 56
    }
  }
})

export default MainTabNavigator
