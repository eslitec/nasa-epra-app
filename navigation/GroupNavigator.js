import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { Icon } from 'expo'
import GroupList from '../screens/GroupList'
import ProfileNavigator from './ProfileNavigator'

const GroupStack = createStackNavigator({
  GroupList,
  Profile: ProfileNavigator
}, {
  headerMode: 'none'
})

GroupStack.navigationOptions = {
  tabBarLabel: 'Group',
  tabBarIcon: ({ focused }) => (
    <Icon.FontAwesome
      name='group'
      size={26}
      style={{ marginBottom: -3 }}
      color={focused ? '#0034b6' : '#979797'}
    />
  )
}

export default GroupStack
